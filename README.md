# Common
## Node.js
Installer la version 4.4.7 de nodejs
https://nodejs.org/en/

# APP MOBILE
## Install Appcelerator Titanium 
Pour installer Titanium
```
#!shell
npm install -g titanium appcelerator alloy gittio
```

Pour avoir le dernier SDK Titanium
```
#!shell
appc ti sdk install
```

Pour avoir un SDK Titanium précis
```
#!shell
appc ti sdk install 5.1.2.GA
```

Setup
```
#!shell
titanium setup
```

Autocompletion
https://github.com/DeSater/jsca2js


## TURBO DEV
You want to enhance your developer experience ? Thus, use tishadow with selective compilation and improve code quality thanks to es6 !
### Launch once
	npm install
	tishadow config --boost

### Launch after each branch changing
	grunt prebuild
	ti build -p {your_platform} --device-id {your_emulator_or_your_device} --appify

### Launch when you are rock star developing
	grunt dev -p {your_platform}

# SERVER