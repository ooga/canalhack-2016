import Joi from 'joi';

export default {
    // POST /api/users
    createUser: {
        body: {
            nickname: Joi.string().required(),
            firstname: Joi.string().required(),
            lastname: Joi.string().required(),
            email: Joi.string().required(),
            hashPassword: Joi.string().required()
        }
    },

    // UPDATE /api/users/:userId
    updateUser: {
        body: {
            nickname: Joi.string().required(),
            firstname: Joi.string().required(),
            lastname: Joi.string().required(),
            email: Joi.string().required(),
            hashPassword: Joi.string().required()
        },
        params: {
            userId: Joi.string().required()
        }
    },

    // GET /api/users/login
    loginUser: {
        query: {
            email: Joi.string().required(),
            hashPassword: Joi.string().required()
        }
    },

    // POST /api/events
    createEvent: {
        body: {
            name: Joi.string().required(),
            datetime: Joi.date().required(),
            place: Joi.any().required(),
            acceptedUsers: Joi.any().required()
        }
    }
};
