import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import eventCtrl from '../controllers/event';

const router = express.Router();	// eslint-disable-line new-cap

router.route('/')
    .get(eventCtrl.list)
    .post(validate(paramValidation.createEvent), eventCtrl.create);

export default router;
