import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai from 'chai';
import { expect } from 'chai';
import app from '../../index';

chai.config.includeStack = true;

describe('## FT : User APIs', () => {
    let givenUser = {
        nickname: 'touchemonbeat',
        firstname: 'Si si',
        lastname: 'On est là',
        email: 'canalhacke-2016@canaltp.fr',
        hashPassword: 'LFEFjzefpojzeFJZEFjoz'
    };

    describe('# POST /api/users', () => {
        it('should create a new user', (done) => {
            request(app)
                .post('/api/users')
                .send(givenUser)
                .expect(httpStatus.OK)
                .then(res => {
                    const responseUser = res.body;
                    expect(responseUser.id).not.to.equal(undefined);
                    expect(responseUser.nickname).to.equal('touchemonbeat');
                    expect(responseUser.firstname).to.equal('Si si');
                    expect(responseUser.lastname).to.equal('On est là');
                    expect(responseUser.email).to.equal('canalhacke-2016@canaltp.fr');
                    expect(responseUser.hashPassword).to.equal(undefined);
                    givenUser = responseUser;
                    done();
                });
        });
    });

    describe('# GET /api/users/:userId', () => {
        it('should get user details', (done) => {
            request(app)
                .get(`/api/users/${givenUser.id}`)
                .expect(httpStatus.OK)
                .then(res => {
                    expect(res.body.email).to.equal(givenUser.email);
                    // expect(res.body.hashPassword).to.equal(undefined);
                    done();
                });
        });

        it('should report error with message - Not found, when user does not exists', (done) => {
            request(app)
                .get('/api/users/56c787ccc67fc16ccc1a5e92')
                .expect(httpStatus.NOT_FOUND)
                .then(res => {
                    expect(res.body.message).to.equal('Not Found');
                    done();
                });
        });
    });

    /*
    describe('# PUT /api/users/:userId', () => {
        it('should update user details', (done) => {
            givenUser.email = 'canalhacke-2016-updated@canaltp.fr';
            request(app)
                .put(`/api/users/${givenUser.id}`)
                .send(givenUser)
                .expect(httpStatus.OK)
                .then(res => {
                    expect(res.body.email).to.equal('canalhacke-2016-updated@canaltp.fr');
                    expect(res.body.hashPassword).to.equal(givenUser.hashPassword);
                    done();
                });
        });
    });
    */

    describe('# GET /api/users/', () => {
        it('should get all users', (done) => {
            request(app)
                .get('/api/users')
                .expect(httpStatus.OK)
                .then(res => {
                    expect(res.body).to.be.an('array');
                    done();
                });
        });
    });

    describe('# DELETE /api/users/', () => {
        it('should delete user', (done) => {
            request(app)
                .delete(`/api/users/${givenUser.id}`)
                .expect(httpStatus.OK)
                .then(res => {
                    // expect(res.body.email).to.equal('canalhacke-2016-updated@canaltp.fr');
                    // expect(res.body.hashPassword).to.equal(givenUser.hashPassword);
                    done();
                });
        });
    });
});
