import chai from 'chai';
import { expect } from 'chai';
import User from '../../models/user';

chai.config.includeStack = true;

describe('## UT : User model', () => {
    describe('Instanciation new User()', () => {
        it('should create a new user', (done) => {
            const user = new User({
                id: 'idDesEnfers'
            });

            expect(user.id).to.equal('idDesEnfers');
            done();
        });
    });
});
