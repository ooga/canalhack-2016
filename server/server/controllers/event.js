import EventRepository from '../repositoryModels/eventRepository';
import Event from '../models/event';

function list(req, res, next) {
    const { limit = 50, skip = 0 } = req.query;
    EventRepository.list({ limit, skip })
        .then((eventsRepository) => {
            const events = [];
            eventsRepository.forEach((eventRepository) =>
                // events.push(Event.fromEventRepositoryToServiceModel(eventRepository))
                events.push(eventRepository)
            );
            res.json(events);
        })
        .error((e) => next(e));
}

function create(req, res, next) {
    const eventRepository = new EventRepository({
        name: req.body.name,
        datetime: req.body.datetime,
        place: req.body.place,
        acceptedUsers: req.body.acceptedUsers
    });

    eventRepository.saveAsync()
        .then((savedEvent) => res.json(
            Event.fromEventRepositoryToServiceModel(savedEvent))
        )
        .error((e) => next(e));
}

export default { list, create };
