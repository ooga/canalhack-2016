import UserRepository from '../repositoryModels/userRepository';
import User from '../models/user';
import Event from '../models/event';
import Place from '../models/place';

/**
 * Load userRepository and append to req.
 */
function load(req, res, next, id) {
    UserRepository.get(id).then((userRepository) => {
        req.userRepository = userRepository;		// eslint-disable-line no-param-reassign
        return next();
    }).error((e) => next(e));
}

/**
 * Get userRepository
 * @returns {UserRepository}
 */
function get(req, res) {
    return res.json(req.userRepository);
}

/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {UserRepository}
 */
function create(req, res, next) {
    UserRepository.canCreateUser(req.body.email)
        .then((canCreate) => {
            if (canCreate) {
                const userRepository = new UserRepository({
                    nickname: req.body.nickname,
                    firstname: req.body.firstname,
                    lastname: req.body.lastname,
                    email: req.body.email,
                    hashPassword: req.body.hashPassword
                });

                userRepository.saveAsync()
                    .then((savedUser) => res.json(User.fromUserRepositoryToServiceModel(savedUser)))
                    .error((e) => next(e));
            }
        })
        .error((e) => next(e));
}

/**
 * Update existing user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {UserRepository}
 */
function update(req, res, next) {
    const userRepository = req.userRepository;
    userRepository.nickname = req.body.nickname;
    userRepository.firstname = req.body.firstname;
    userRepository.lastname = req.body.lastname;
    userRepository.email = req.body.email;
    userRepository.hashPassword = req.body.hashPassword;
    userRepository.lastModified = req.body.lastModified;

    userRepository.saveAsync()
        .then((savedUser) => res.json(User.fromUserRepositoryToServiceModel(savedUser)))
        .error((e) => next(e));
}

/**
 * Get userRepository list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {UserRepository[]}
 */
function list(req, res, next) {
    const { limit = 50, skip = 0 } = req.query;
    UserRepository.list({ limit, skip })
        .then((usersRepository) => {
            const users = [];
            usersRepository.forEach((userRepository) =>
                users.push(User.fromUserRepositoryToServiceModel(userRepository))
            );
            res.json(users);
        })
        .error((e) => next(e));
}

/**
 * Delete user.
 * @returns {UserRepository}
 */
function remove(req, res, next) {
    const userRepository = req.userRepository;
    userRepository.removeAsync()
        .then((deletedUser) => res.json(User.fromUserRepositoryToServiceModel(deletedUser)))
        .error((e) => next(e));
}

/**
 * Get events of user.
 * @returns [Event]
 */
function getEvents(req, res) {
    const currentDate = new Date();
    res.json([
        new Event({
            name: 'Le pot quotidien, toi même tu sais',
            place: new Place({
                id: '1',
                name: 'Rossli',
                label: '1 Rue d\'Aligre, 75012 Paris',
                coord: {
                    lat: 48.847767,
                    lon: 2.37721
                }
            }),
            id: '1',
            datetime: currentDate,
            acceptedUsers: [
                new User({
                    id: '1',
                    nickname: 'Jean-Michel Padechute',
                    firstname: 'Paul',
                    lastname: 'Poule'
                })
            ],
            declinedUsers: [
                new User({
                    id: '2',
                    nickname: 'Bruno Lingette',
                    firstname: 'Castor',
                    lastname: 'Chafouin'
                })
            ],
            waitingUsers: []
        }),
        new Event({
            name: 'La kermesse mensuel ouais ouais',
            place: new Place({
                id: '2',
                name: 'Le Motel',
                label: '8 Passage Josset, 75011 Paris',
                coord: {
                    lat: 48.852733,
                    lon: 2.376414
                }
            }),
            id: '2',
            datetime: currentDate,
            acceptedUsers: [],
            declinedUsers: [
                new User({
                    id: '1',
                    nickname: 'Jean-Michel Padechute',
                    firstname: 'Paul',
                    lastname: 'Poule'
                })
            ],
            waitingUsers: [
                new User({
                    id: '3',
                    nickname: 'Michel Palourdiou',
                    firstname: 'Pascal',
                    lastname: 'Legrandfrere'
                })
            ]
        })
    ]);
}

function login(req, res, next) {
    UserRepository.getByEmailAndHashPassword(req.query.email, req.query.hashPassword)
        .then((userRepository) => res.json(User.fromUserRepositoryToServiceModel(userRepository)))
        .error((e) => next(e));
}

export default { load, get, create, update, list, remove, getEvents, login };
