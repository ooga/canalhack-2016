import mongoose from 'mongoose';

/**
 * User Schema
 */
export const EventSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    datetime: {
        type: Date,
        default: Date.now
    },
    place: {
        name: {
            type: String,
            required: true
        },
        label: {
            type: String,
            required: true
        },
        coord: {
            lat: {
                type: Number,
                required: true
            },
            lon: {
                type: Number,
                required: true
            }
        }
    },
    acceptedUsers: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'UserRepository'
    },
    declinedUsers: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'UserRepository'
    },
    waitingUsers: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'UserRepository'
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    lastModified: {
        type: Date,
        default: Date.now
    }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
EventSchema.method({});

/**
 * Statics
 */
EventSchema.statics = {
    list({ skip = 0, limit = 50 } = {}) {
        return this.find()
            .sort({ createdAt: -1 })
            .skip(skip)
            .limit(limit)
            .execAsync();
    }
};

/**
 * @typedef EventRepository
 */
export default mongoose.model('EventRepository', EventSchema);
