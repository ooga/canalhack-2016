import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * User Schema
 */
export const UserSchema = new mongoose.Schema({
    nickname: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    hashPassword: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    lastModified: {
        type: Date,
        default: Date.now
    }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
UserSchema.method({});

/**
 * Statics
 */
UserSchema.statics = {
    /**
     * Get userRepository
     * @param {ObjectId} id - The objectId of userRepository.
     * @returns {Promise<UserRepository, APIError>}
     */
    get(id) {
        return this.findById(id)
            .execAsync().then((userRepository) => {
                if (userRepository) {
                    return userRepository;
                }
                const err = new APIError('No such userRepository exists!', httpStatus.NOT_FOUND);
                return Promise.reject(err);
            });
    },

    /**
     * Get userRepository
     * @param {ObjectId} id - The objectId of userRepository.
     * @returns {Promise<UserRepository, APIError>}
     */
    canCreateUser(currentEmail) {
        return this.findOne(
            {
                email: currentEmail
            })
            .execAsync().then((userRepository) => {
                if (userRepository) {
                    const err = new APIError('User already exists!', httpStatus.CONFLICT);
                    return Promise.reject(err);
                }
                return true;
            });
    },

    /**
     * Get userRepository
     * @param {ObjectId} id - The objectId of userRepository.
     * @returns {Promise<UserRepository, APIError>}
     */
    getByEmailAndHashPassword(currentEmail, currentHashPassword) {
        return this.findOne(
            {
                email: currentEmail,
                hashPassword: currentHashPassword
            })
            .execAsync().then((userRepository) => {
                if (userRepository) {
                    return userRepository;
                }
                const err = new APIError('No such userRepository exists!', httpStatus.NOT_FOUND);
                return Promise.reject(err);
            });
    },

    /**
     * List users in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of users to be skipped.
     * @param {number} limit - Limit number of users to be returned.
     * @returns {Promise<UserRepository[]>}
     */
    list({ skip = 0, limit = 50 } = {}) {
        return this.find()
            .sort({ createdAt: -1 })
            .skip(skip)
            .limit(limit)
            .execAsync();
    }
};

/**
 * @typedef UserRepository
 */
export default mongoose.model('UserRepository', UserSchema);
