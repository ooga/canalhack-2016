import windowLib from 'view/window';
import User from 'entity/user';
import DbClient from 'ws/db';

class HomeController {

    constructor() {
        $.btn_login.addEventListener('click', () => { this.login() });
        $.btn_signin.addEventListener('click', () => { this.signin() });
    }

    login() {
        let client = new DbClient();
        client.connect($.email.value, User.encodePassword($.password.value), (e) => {
            let user = JSON.parse(client.getResponseText());
            Ti.App.Properties.setObject('user', user);
            windowLib.open(Alloy.createController('events').getView());
        }, (e) => {
            $.lbl_error.text = "Try again !!!"
        });
    }

    signin() {
        windowLib.open(Alloy.createController('signin').getView());
    }
}

new HomeController();
