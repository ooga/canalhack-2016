import windowLib from 'view/window';
import DbClient from 'ws/db';

class EventsController {

    constructor() {
        let user = Ti.App.Properties.getObject('user');
        this.client = new DbClient();
        this.client.getEvents(user.id, this.successCallback);
        $.listView.addEventListener('itemclick', (e) => {
            let event = e.section.getItemAt(e.itemIndex).data;
            windowLib.open(Alloy.createController('event/details', event).getView());
        });
        $.btn_create.addEventListener('click', (e) => {
            windowLib.open(Alloy.createController('event/create').getView());
        });
    }

    successCallback(e) {
        let items = [];
        let events = JSON.parse(this.client.getResponseText());
        events.forEach(function(event) {
            items.push({
                properties: {
                    title: event.name,
                    color: '#968b8b',
                    left: 10,
                },
                data: event,
            });
        });
        $.section.setItems(items);
    }
}

new EventsController();
