import windowLib from 'view/window';
import User from 'entity/user';
import DbClient from 'ws/db';

class SigninController {

    constructor() {
        $.btn_signin.addEventListener('click', () => { this.signin() });

        $.getView().addEventListener("open", (e) => {
            let actionBar = $.getView().activity.actionBar;
            if (actionBar) {
                actionBar.onHomeIconItemSelected = this.close;
            }
        });
    }

    signin() {
        let data = {};
        data.nickname = $.nickname.value;
        data.firstname = $.firstname.value;
        data.lastname = $.lastname.value;
        data.email = $.email.value;
        data.hashPassword = User.encodePassword($.password.value);
        let user = new User(data);

        let client = new DbClient();
        client.createUser(user, (e) => {
            let user = JSON.parse(client.getResponseText());
            Ti.App.Properties.setObject('user', user);
            windowLib.open(Alloy.createController('events').getView());
            this.close();
        });
    }

    close() {
        $.getView().close();
        $.destroy();
    }
}

new SigninController();
