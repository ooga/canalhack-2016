import Place from 'entity/place';
import DbClient from 'ws/db';
import AutocompleteClient from 'ws/autocomplete';

class CreateEventController {

    constructor(connectedUser) {
        $.getView().addEventListener("open", (e) => {
            let actionBar = $.getView().activity.actionBar;
            if (actionBar) {
                actionBar.onHomeIconItemSelected = this.close;
            }
        });

        // Location autocomplete
        let location;
        let autocompeteClient = new AutocompleteClient();
        $.location.on('change', function(e) {
            autocompeteClient.getSuggestions(e.value, function(e) {
                let datas = JSON.parse(autocompeteClient.getResponseText());
                let places = [];
                datas.features.forEach(function(data) {
                    let place = Place.hydrate(data)
                    place.title = place.name;
                    places.push(place);
                });
                $.location.setSuggestions(places);
            });
        });
        $.location.on('exportData', function(data) {
            delete data.title;
            location = data;
        });

        // Datetime selection
        this.date = new Date();
        this.date.setHours(20, 0, 0, 0);
        $.lbl_date.addEventListener('click', () => {
            this.showDate();
        });
        $.lbl_time.addEventListener('click', () => {
            this.showTime();
        });
        this.refreshDatetimeLabels();

        // Guest selection
        let selectedUsers = {};
        new DbClient().getUsers(function() {
            let users = JSON.parse(this.client.getResponseText());
            users.forEach(function(user) {
                if (connectedUser.id !== user.id) {
                    let checkbox = Ti.UI.createSwitch({
                        style: Ti.UI.Android.SWITCH_STYLE_CHECKBOX,
                        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
                        title: user.firstname + ' ' + user.lastname,
                        width: Ti.UI.FILL,
                        user: user
                    });
                    checkbox.addEventListener('change', function(e) {
                        if (e.source.value) {
                            selectedUsers[e.source.user.id] = e.source.user;
                        } else {
                            delete selectedUsers[e.source.user.id];
                        }
                    });
                    $.guests.add(checkbox);
                }
            });
        });

        // Create event
        $.btn_add.addEventListener('click', this.create);
    }

    create() {
        console.log("nothing yet !!!");
    }

    showDate() {
        Ti.UI.createPicker({
            type: Ti.UI.PICKER_TYPE_DATE
        }).showDatePickerDialog({
            value: this.date,
            callback: (e) => {
                if (!e.cancel) {
                    let newDate = e.value;
                    this.date.setFullYear(newDate.getFullYear(), newDate.getMonth(), newDate.getDate());
                    this.refreshDatetimeLabels();
                }
            }
        });
    }

    showTime() {
        Ti.UI.createPicker({
            type: Ti.UI.PICKER_TYPE_TIME
        }).showTimePickerDialog({
            value: this.date,
            callback: (e) => {
                if (!e.cancel) {
                    let newDate = e.value;
                    this.date.setHours(newDate.getHours(), newDate.getMinutes());
                    this.refreshDatetimeLabels();
                }
            }
        });
    }

    refreshDatetimeLabels() {
        $.lbl_date.text = this.date.toLocaleDateString();
        $.lbl_time.text = this.date.toLocaleTimeString();
    }

    close() {
        $.getView().close();
        $.destroy();
    }
}

new CreateEventController(Ti.App.Properties.getObject('user'));
