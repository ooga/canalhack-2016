import moment from 'alloy/moment';

class DetailsEventController {

    constructor(event) {
        $.getView().addEventListener("open", (e) => {
            let actionBar = $.getView().activity.actionBar;
            if (actionBar) {
                actionBar.onHomeIconItemSelected = this.close;
            }
        });

        $.lbl_eventname.text =event.name;
        $.lbl_eventplacename.text =event.place.name;
        $.lbl_eventadress.text =event.place.label;
        $.lbl_date.text = moment(event.datetime).format('LLLL');
        [event.acceptedUsers, event.waitingUsers, event.declinedUsers].forEach((usersArray, i) => {
            let color;
            switch (i) {
                case 0:
                    color = '#5DD3B0';
                    break;
                case 2:
                    color = '#FF6E6F';
                    break;
            }
            usersArray.forEach((user) => {
                this.renderUser(user, color);
            });
        });
    }

    renderUser(user, color = '#777') {
        $.section_guests.appendItems([{
            icon: {
                color: color
            },
            name: {
                text: user.nickname
            }
        }]);
    }

    close() {
        $.getView().close();
        $.destroy();
    }
}

new DetailsEventController($.args);
