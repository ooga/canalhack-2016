import windowLib from 'view/window';

class IndexController {

    constructor() {
        let controller;
        if (Ti.App.Properties.hasProperty('user')) {
            controller = Alloy.createController('events');
        } else {
            controller = Alloy.createController('home');
        }
        windowLib.open(controller.getView());
    }
}

new IndexController();
