export default class Place {
    constructor(place) {
        this.id = place.id;
        this.name = place.name;
        this.label = place.label;
        this.coord = place.coord;
    }

    static hydrate(data) {
        let place = {};
        let [lon, lat] = data.geometry.coordinates;
        place.name = data.properties.name;
        place.label = data.properties.name;
        place.coord = {
            "lon": lon,
            "lat": lat
        };
        return new Place(place);
    }
}
