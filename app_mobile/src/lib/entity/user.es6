export default class User {
    constructor(user) {
        this.id = user.id;
        this.nickname = user.nickname;
        this.firstname = user.firstname;
        this.lastname = user.lastname;
        this.email = user.email;
        this.hashPassword = user.hashPassword;
        this.lastModified = user.lastModified;
    }

    static encodePassword(password) {
        const salt = 'MySaltIsStatic';
        let CryptoJS = require('ts.cryptojs256');
        let result = CryptoJS.HmacSHA256(password, salt);
        return result.toString();
    }
}
