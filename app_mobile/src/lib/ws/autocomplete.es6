const baseUrl = "http://photon.komoot.de/api/";

export default class AutocompleteClient {

    constructor() {
        this.client = Ti.Network.createHTTPClient({
            // function called when the response data is available
            onload: (e) => this.successCallback(e),
            // function called when an error occurs, including a timeout
            onerror: (e) => this.failureCallback(e),
        });
    }

    getResponseText() {
        return this.client.responseText;
    }

    call(url) {
        // Prepare the connection.
        this.client.open('GET', url);
        // Send the request.
        this.client.send();
    }

    getSuggestions(query, callback) {
        this.client.abort();
        let url = baseUrl + '?q=' + query;
        this.successCallback = callback;
        this.call(url);
    }

    successCallback(e) {
        Ti.API.debug(e);
    }

    failureCallback(e) {
        Ti.API.debug(e.error);
    }
}
