const baseUrl = "http://ns320671.ip-5-135-162.eu:3000/api/";

export default class DbClient {

    constructor() {
        this.client = Ti.Network.createHTTPClient({
            // function called when the response data is available
            onload: (e) => this.successCallback(e),
            // function called when an error occurs, including a timeout
            onerror: (e) => this.failureCallback(e),
        });
    }

    getResponseText() {
        return this.client.responseText;
    }

    call(method, url, data = {}) {
        // Prepare the connection.
        this.client.open(method, url);
        // Send the request.
        this.client.send(data);
    }

    getEvents(userId, callback) {
        let url = baseUrl + "users/" + userId + "/events";
        this.successCallback = callback;
        this.call('GET', url);
    }

    getUsers(callback) {
        let url = baseUrl + "users/";
        this.successCallback = callback;
        this.call('GET', url);
    }

    createUser(user, success, failure) {
        let url = baseUrl + "users/";
        this.successCallback = success;
        if (failure) {
            this.failureCallback = failure;
        }
        this.call('POST', url, user);
    }

    connect(email, hashPassword, success, failure) {
        let url = baseUrl + "users/login";
        this.successCallback = success;
        if (failure) {
            this.failureCallback = failure;
        }
        this.call('GET', url, {
            email: email,
            hashPassword: hashPassword,
        });
    }

    successCallback(e) {
        Ti.API.debug(e);
    }

    failureCallback(e) {
        Ti.API.debug(e.error);
    }
}
