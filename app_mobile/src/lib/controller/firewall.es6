import windowLib from 'view/window';

export default class FirewallController {

    isLogged() {
        let props = Ti.App.Properties;
        return props.hasProperty('logged') && props.getBool('logged') === true;
    }

    restrictToAnonymous(controller) {
        controller.getView().addEventListener('focus', this.closeIfAnonymous);
        controller.getView().addEventListener('androidFocus', this.closeIfAnonymous);
    }

    closeIfAnonymous() {
        if (this.isLogged()) {
            this.destroy()
        }
    }

}