// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
Alloy.Globals.UI = {};
Alloy.Globals.UI.Button = {};
Alloy.Globals.UI.Button.Height = 50;

var Styles = require('nl.fokkezb.button/styles');
Styles.set('material', {
    backgroundColor: '#FF6E6F',
    color: '#FFF',
    iconFont: 'whozin',
    width: Ti.UI.FILL,
    height: Alloy.Globals.UI.Button.Height,
    iconSize: 25,
    spacing: 10,
});
Styles.setDefault('material');